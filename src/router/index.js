import { createRouter, createWebHistory } from 'vue-router'
import DashboardView from '@/views/DashboardView.vue'
import ItineraryView from '@/views/ItineraryView.vue'
import StationView from '@/views/StationView.vue'
import ProfileView from "@/views/ProfileView.vue";
import ItineraryEditView from "@/views/ItineraryEditView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: DashboardView
    },
    {
      path: '/itinerary',
      name: 'itinerary',
      component: ItineraryView
    },
    {
      path: '/itinerary/:index',
      name: 'itinerary_edit',
      component: ItineraryEditView,
      props: (route) => ({
        index: Number(route.params.index),
      })
    },
    {
      path: '/stations',
      name: 'stations',
      component: StationView
    },
    {
      path: '/settings',
      name: 'settings',
      component: ProfileView
    }
  ]
})

export default router
