import defaultOptions from '../default/options.js'
import { defaultRequestInterceptor, defaultResponseInterceptor } from '../default/interceptors.js'
import DefaultMethods from '../default/methods.js'

/* --------------- Service static configuration ------------- */
// Axios requests settings
const config = {
    ...defaultOptions,
    timeout: 0,
    // xsrfCookieName: 'csrftoken',
    // xsrfHeaderName: 'x-csrftoken',
    // withCredentials: true,
}

// API base URL
const baseURL = {
    protocol: import.meta.env.VITE_APP_PDF_PROTOCOL,
    host: import.meta.env.VITE_APP_PDF_HOST,
    port: import.meta.env.VITE_APP_PDF_PORT,
    root: import.meta.env.VITE_APP_PDF_ROOT,
}

// API models URLs
const modelURLs = {
    itinerary: "/itinerary/",
    itineraryDownload: "/itinerary/download/",
    itineraries: "/itineraries/"
}

// API not-model URLs ("special")
const specialURLs = {
}
/* --------------- Interceptors customization ------------ */
const interceptors = {
    request: { ...defaultRequestInterceptor }, // We inject the default interceptors as needed
    response: { ...defaultResponseInterceptor }
}

/* --------------- Model methods ------------ */
/*  Here the default base CRUD methods are added.
    Custom model methods (other actions than CRUD) can be added in `custom`    */
const modelMethods = (handler, modelName, url) => {
    // DefaultsMethods is BaseModel
    const baseMethods = new DefaultMethods(handler, url)
    const custom = {
        //... add custom method here
        itinerary: {
            // WHY DO WE HAVE TO SEND THE FUCKING TOKEN IN BODY ???????
            delete: (id, payload) => handler.post(url + 'delete/' + id + '/', payload)
        }
    }
    return { ...baseMethods, ...custom[modelName] }
}

/* ---------------- "Special" methods ---------- */
/*  Special methods are API calls that do not need CRUD.
    They still benefit from url prefixing               */
const specialMethods = (handler, specialName, url) => {
    const special = {
        //... add special method here
    }
    return special[specialName]
}

export default {
    config,
    baseURL,
    interceptors,
    modelURLs,
    specialURLs,
    modelMethods,
    specialMethods
}
