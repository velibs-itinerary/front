import defaultOptions from '../default/options.js'
import { defaultRequestInterceptor, defaultResponseInterceptor } from '../default/interceptors.js'
import DefaultMethods from '../default/methods.js'

/* --------------- Service static configuration ------------- */
// Axios requests settings
const config = {
    ...defaultOptions,
    // xsrfCookieName: 'csrftoken',
    // xsrfHeaderName: 'x-csrftoken',
    // withCredentials: true,
}

// API base URL
const baseURL = {
    protocol: import.meta.env.VITE_APP_API_PROTOCOL,
    host: import.meta.env.VITE_APP_API_HOST,
    port: import.meta.env.VITE_APP_API_PORT,
    root: import.meta.env.VITE_APP_API_ROOT,
}

// API models URLs
const modelURLs = {
}

// API not-model URLs ("special")
const specialURLs = {
    auth: '/auth/',
    user: ''
}
/* --------------- Interceptors customization ------------ */
const interceptors = {
    request: { ...defaultRequestInterceptor }, // We inject the default interceptors as needed
    response: { ...defaultResponseInterceptor }
}

/* --------------- Model methods ------------ */
/*  Here the default base CRUD methods are added.
    Custom model methods (other actions than CRUD) can be added in `custom`    */
const modelMethods = (handler, modelName, url) => {
    const baseMethods = new DefaultMethods(handler, url)
    const custom = {
        //... add custom method here
    }
    return { ...baseMethods, ...custom[modelName] }
}

/* ---------------- "Special" methods ---------- */
/*  Special methods are API calls that do not need CRUD.
    They still benefit from url prefixing               */
const specialMethods = (handler, specialName, url) => {
    const special = {
        auth: {
            // login
            login: (payload) => handler.post(url + 'login/', payload),
            // register
            register: (payload) => handler.post(url + 'register/', payload),
            // get credentials of user by this token
            verify: (payload) => handler.post(url + 'verify/', payload),
            // logout
            logOut: (payload) => handler.post(url + 'logout/', payload),
        },
        user: {
            editCredentials: (payload, id) => handler.patch(url + 'user/' + id, payload)
        }
    }
    return special[specialName]
}

export default {
    config,
    baseURL,
    interceptors,
    modelURLs,
    specialURLs,
    modelMethods,
    specialMethods
}
