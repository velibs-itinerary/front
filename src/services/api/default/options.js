const defaultOptions = {
    timeout: 10000,
    headers: {
        common: {
            'X-Requested-With': 'XMLHttpRequest', 
        }
    },
}

export default defaultOptions

