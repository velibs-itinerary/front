import axios from 'axios'

class BaseModel {
    constructor(handler, url) {
        this.handler = handler
        this.baseURL = url
    }

    search = (query) => this.handler.get(this.baseURL, { params: query })
    getByID = (id) => this.handler.get(this.baseURL + id + '/')
    optionsByID = (id) => this.handler.options(this.baseURL + id + '/')
    create = (payload) => this.handler.post(this.baseURL, payload)
    // I still don't know why we're pushing the token into the body, so we need to do ugly things
    post = (payload) => this.handler.post(this.baseURL, payload)
    update = (id, payload) => this.handler.put(this.baseURL + id + '/', payload)
    destroy = (id, payload) => this.handler.delete(this.baseURL + id + '/', payload)


    /**
     * Tricks to edit options of axios
     * @param {*} options 
     * @return Axios instance
     * 
     * Example : 
     * let newInstance = this.$api.model.setOptions({...})
     * newInstance.search()...
     */
    setOptions = (options) => {
        const newConfig = {
            ...this.handler.defaults,
            ...options
        }
        // return a new instance of axios
        this.handler = axios.create(newConfig)

        return this
    }
}

export default BaseModel