import axios from 'axios'

/*
  This parses the "api" folder and
  considers every "*.service.js" file as an enabled service.
  ⚠ Please chose service name short and comprehensible and avoid ES keywords.
*/

const availableAPIs = import.meta.glob('./**/*.service.js', { eager: true })

/*
  All found services are initialized
*/
const init = (app) => {
    // availableAPIs.keys().forEach(filename => {
    for (const [path, objectMethods] of Object.entries(availableAPIs)) {
        /* ------- API network configuration ---------- */
        const apiDescription = objectMethods

        const serviceName = path.match(/\/([^/]+)\.service.js$/)[1]
        // We grab the base URL for this API
        const url = apiDescription.default.baseURL
        // Interceptors
        const interceptors = apiDescription.default.interceptors
        // Base URL reconstitution
        const baseURL = [
            url.protocol,
            '://',
            url.host,
            (['', undefined, '80', 80].includes(url.port)) ? '' : ':' + url.port,
            url.root
        ].join('')

        /* ----- Axios instance configuration ----- */
        const config = { ...apiDescription.default.config, baseURL: baseURL }
        const _axios = axios.create(config)
        _axios.interceptors.request.use(interceptors.request.configInterceptor, interceptors.request.errorInterceptor)
        _axios.interceptors.response.use(interceptors.response.responseInterceptor, interceptors.response.errorInterceptor)

        /* ----- API modelization ------------ */
        // "Models" get CRUD and custom methods, "special" for not-model functionalities
        const modelURLs = apiDescription.default.modelURLs
        const specialURLs = apiDescription.default.specialURLs

        const methods = {}
        Object.keys(modelURLs).forEach(k => {
            const modelMethods = apiDescription.default.modelMethods(_axios, k, modelURLs[k])
            methods[k] = modelMethods
        })
        Object.keys(specialURLs).forEach(k => {
            const specialMethods = apiDescription.default.specialMethods(_axios, k, specialURLs[k])
            methods[k] = specialMethods
        })
        console.debug('SERVICES: available methods', methods)

        // script setup need to provide service
        console.log("serviceName", serviceName.toLowerCase());
        app.provide(['$' + serviceName.toLowerCase()], methods)
    }
}

export default {
    init
}

