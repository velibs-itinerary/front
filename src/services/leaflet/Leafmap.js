import 'leaflet';
import 'leaflet-routing-machine'
import 'leaflet.markercluster'
import 'leaflet-control-geocoder'
import './Leaflet.BigImage'

import {LEAFLET_DEFAULT_DATA} from "@/services/leaflet/default_data";

import {v4 as uuidv4} from 'uuid';

export class Leafmap {
    constructor(el = 'map', options = {}) {
        this.el = document.getElementById(el);
        this.options = options;

        this.itinerarySteps = [];

        this.map = null;
        this.printer = null;
        this.geocoder = null;
        this.markers = null;
        this.routing = null;
        this.contextMenu = null;

        this.dataUrl = 'https://opendata.paris.fr/api/records/1.0/search/?dataset=velib-disponibilite-en-temps-reel&q=&rows=10000&facet=name&facet=is_installed&facet=is_renting&facet=is_returning&facet=nom_arrondissement_communes';

        // API_KEY Mapbox
        this.APY_KEY = 'pk.eyJ1IjoiZmFjdG9yc3BlZWQiLCJhIjoiY2xqc2ZjNnh6MDc2ZzNscDlhb3g0d2JkYiJ9.qJmLGleZVJT2euqe7IlR7w';

        // Constants
        this.ITINERARY_TYPE_MAP = 'MAP';
        this.ITINERARY_TYPE_MARKER = 'MARKER';

        this.defaultLat = 48.8643713;
        this.defaultLng = 2.3300615;

        this.PUBLIC_PATH = 'leaflet/'
        this.PUBLIC_MARKER_PATH = this.PUBLIC_PATH + 'marker/'
        this.PUBLIC_DIRECTION_PATH = this.PUBLIC_PATH + 'direction/'

        this.MARKER_ICON_BLUE = new URL("../../../public/" + this.PUBLIC_MARKER_PATH + 'marker-icon-blue.png', import.meta.url).href
        
        this.MARKER_ICON_ROSE = new URL("../../../public/" + this.PUBLIC_MARKER_PATH + 'marker-icon-rose.png', import.meta.url).href
        this.MARKER_SHADOW = new URL("../../../public/" + this.PUBLIC_MARKER_PATH + 'marker-shadow.png', import.meta.url).href

        this.AVAILABLE_ICONS = [
            'Straight',
            'SlightRight',
            'Right',
            'SharpRight',
            'TurnAround',
            'Uturn',
            'SharpLeft',
            'Left',
            'SlightLeft',
            'Waypoint',
            'Roundabout',
            'EnterAgainstAllowedDirection',
            'LeaveAgainstAllowedDirection',
            'Continue',
            'EndOfRoad',
            'Fork'
        ];

        this.data = null;

        this.init();
    }

    getTagImage() {
        return document.getElementById('ui_image_travel');
    }

    async init() {
        this.map = this.initMap();

        this.geocoder = this.initGeocoder();

        this.routing = this.initRoutingControl()
        this.routing.on('routesfound', (e) => this.onRoutingRoutesFound(e));
        this.map.addControl(this.routing)

        this.printer = this.initPrinter();

        this.markers = this.initMarkerCluster();

        let records = await this.initData();
        
        records.forEach((record) => {
            let fields = record['fields'];
            let title = fields['name'];
            let coordinates = fields['coordonnees_geo'];
            let uuid = uuidv4();

            let latlng = L.latLng(coordinates[0], coordinates[1]);

            let marker = this.createMarker({
                type: this.ITINERARY_TYPE_MARKER,
                latlng: latlng,
                title: title,
                iconSelected: false,
                uuid: uuid
            });

            this.markers.addLayer(marker);
        })

        this.map.addLayer(this.markers);

        this.map.on('click', (e) => this.onMapClick(e));

        this.initImage();

        this.on('init')
    }

    initImage() {
        const observer = new MutationObserver(this.mutationCallback);
        observer.observe(this.getTagImage(), {attributes: true})
    }

    mutationCallback = (mutationsList) => {
        for (const mutation of mutationsList) {
            if (
                mutation.type !== "attributes" ||
                mutation.attributeName !== "data-base64"
            ) {
                return
            }

            this.data.image = mutation.target.getAttribute("data-base64");

            this.on('userAction');
        }
    }

    /**

     * @returns 
     */
    async initData() {
        // The api of https://opendata.paris.fr is not stable
        // During dev i've got somes troubles
        // So we return default data in case of
        return await fetch(this.dataUrl, {
            method: 'GET',
            headers: new Headers()
        })
        .then((res) => res.json())
        .then((res) => res.record())
        .catch(() => LEAFLET_DEFAULT_DATA.records)
    }

    initMarkerCluster() {
        return new L.MarkerClusterGroup({
            maxClusterRadius: 120,
            spiderfyOnMaxZoom: true,
            showCoverageOnHover: false,
            zoomToBoundsOnClick: true
        })
    }

    initTile() {
        return L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            maxZoom: 19
        });
    }

    initMap() {
        let latlng = L.latLng(this.defaultLat, this.defaultLng);

        return L.map(this.el, {
            center: latlng,
            zoom: 11.04,
            layers: [this.initTile()]
        })
    }

    initGeocoder() {
        return L.Control.Geocoder.nominatim();
    }

    initPrinter() {
        return L.control.bigImage({
            manualPrint: true,
            display: this.getTagImage()
        }).addTo(this.map)
    }

    initRoutingControl() {
        return L.Routing.control({
            fitSelectedRoutes: true,
            draggableWaypoints: false,
            routeWhileDragging: false,
            createMarker: function () {
                return null;
            },
            lineOptions: {
                addWaypoints: false,
                // styles: [{color: '#ff0000', weight: 2}]
            },
            router: L.Routing.mapbox(this.APY_KEY, {profile: 'mapbox/cycling'}),
            plan: new L.Routing.plan([], {
                addWaypoints: false,
                draggableWaypoints: false,
                createMarker: () => undefined
            }),
            collapsible: true,
            show: false
        })
    }

    setWaypoints() {
        this.resetUx();

        let points = [];

        if (this.itinerarySteps.length > 1) {
            points = this.itinerarySteps.map((itinerary) => {
                return itinerary.latlng;
            })
        }

        if (this.itinerarySteps.length === 1) {
            this.createDefaultUx()
        }

        this.routing.setWaypoints(points);
    }

    getSteps(route) {
        let steps = []

        let n = 0;
        route.instructions.forEach((instruction) => {
            if (instruction.type === 'WaypointReached') return;

            let type = instruction.type;
            let distance = instruction.distance;
            let time = instruction.time;
            let direction = instruction.direction;
            let text = instruction.text;
            let road = instruction.road;
            let lat = null;
            let lng = null;

            if (type === 'Head' || type === 'DestinationReached') {
                type = 'Waypoint';
                text = this.itinerarySteps[n].title ?? '';
                road = text;

                let coordinates = route['inputWaypoints'][n].latLng;
                lat = coordinates.lat;
                lng = coordinates.lng;

                n++;
            }

            steps.push({
                type: type,
                distance: distance,
                time: time,
                direction: direction,
                text: text,
                road: road,
                lat: lat,
                lng: lng
            })
        });

        return steps;
    }

    generateRouteData(route) {
        this.data = {
            'distance': route.summary.totalDistance,
            'distanceUnit': 'm',
            'time': route.summary.totalTime,
            'timeUnit': 's',
            'steps': this.getSteps(route),
            'image': undefined
        }

        this.createUx(this.data);
    }

    createDefaultUx() {
        let step = this.itinerarySteps[0];
        step.type = 'Waypoint';
        step.distance = 0;

        document.getElementById('ui_from_destination').innerHTML = step.text

        document.getElementById('ui_table_steps')
            .querySelector('tbody')
            .insertAdjacentHTML('beforeend', this.ui_table_row(step))
    }

    createUx(routes) {
        let steps = routes.steps;

        let from_destination = this.ui_from_destination(steps)
        let to_destination = this.ui_to_destination(steps)
        let steps_distance = this.ui_convert_to_km(routes.distance);
        let steps_time = this.ui_seconds_to_hours(routes.time);

        document.getElementById('ui_from_destination').innerHTML = from_destination.text
        document.getElementById('ui_to_destination').innerHTML = to_destination.text

        document.getElementById('ui_total_travel').innerHTML = `${steps_distance}, ${steps_time}`

        routes.steps.forEach((step) => {
            document.getElementById('ui_table_steps')
                .querySelector('tbody')
                .insertAdjacentHTML('beforeend', this.ui_table_row(step))
        })
    }

    getLatLngForImage(coordinates) {
        let lat = 0;
        let lng = 0;

        coordinates.forEach((coordinate) => {
            lat = Math.max(coordinate.lat, lat);
            lng = Math.max(coordinate.lng, lng);
        })

        lat = lat < 0 ? lat += 3 : lat -= 2;
        lng = lng < 0 ? lng += 3 : lng -= 2;

        return L.latLng(lat, lng);
    }

    createPopup(options) {
        return L.popup(options.e.latlng, {
            content: options.e.target.options.title,
            autoClose: options.autoClose ?? true,
            offset: L.point(0, -30)
        }).openOn(this.map)
    }

    async getReverseGeocode(coordinates) {
        return new Promise((resolve, reject) => {
            this.geocoder.reverse(coordinates, this.map.getZoom(), (results) => {
                if (results && results.length > 0) {
                    resolve(results);
                } else {
                    reject(new Error('Aucune adresse trouvée.'));
                }
            });
        });
    }

    createIcon(selected = false) {
        return L.icon({
            iconUrl: selected ? this.MARKER_ICON_ROSE : this.MARKER_ICON_BLUE,
            shadowUrl: this.MARKER_SHADOW,
            iconSize: [25, 41],
            shadowSize: [25, 41]
        });
    }

    createMarker(options = {}) {
        let marker = new L.Marker(options.latlng, {
            type: options.type,
            title: options.title,
            alt: options.title,
            icon: this.createIcon(options.iconSelected),
            uuid: options.uuid
        });

        marker.on('click', (e) => this.onMarkerClick(e));
        marker.on('mouseover', (e) => this.onMarkerOver(e));
        marker.on('mouseout', (e) => this.onMarkerOut(e));
        marker.on('contextmenu', (e) => this.onMarkerContextMenu(e));

        return marker;
    }

    resetContextMenu() {
        if (this.contextMenu) {
            this.contextMenu.close();
            this.contextMenu = null;
        }
    }

    createContextMenu(evMarker, marker) {
        let menu = document.createElement('div');

        if (evMarker.target.options.type === this.ITINERARY_TYPE_MAP) {
            menu.append(this.createContextMenuMap(evMarker, marker));
        }

        if (evMarker.target.options.type === this.ITINERARY_TYPE_MARKER) {
            menu.append(this.createContextMenuMarker(evMarker, marker))
        }

        return menu;
    }

    createContextMenuMap(evMarker, marker) {
        let p = document.createElement('p');

        let del = document.createElement('button');
        del.innerText = 'Supprimer le marker';
        del.addEventListener('click', () => this.onMarkerRemove(evMarker, marker))

        p.append(del);

        return p;
    }

    createContextMenuMarker(evMarker, marker) {
        let p = document.createElement('p');

        let del = document.createElement('button');
        del.innerText = 'Supprimer le tracer';
        del.addEventListener('click', () => this.onMarkerRemove(evMarker, marker))

        p.append(del);

        return p;
    }

    onMarkerContextMenu(e) {
        this.resetContextMenu();
        this.onMarkerOut(e)

        let _marker = null

        this.itinerarySteps.forEach((itinerary, index) => {
            if (itinerary.uuid === e.target.options.uuid) {
                _marker = {...itinerary, index: index};
            }
        });

        if (_marker) {
            this.contextMenu = this.createPopup({e: e, autoClose: false})

            this.contextMenu.setContent(this.createContextMenu(e, _marker))

            this.contextMenu.on('remove', () => this.onContextMenuRemove())
        }
    }

    async onMapClick(e) {
        this.resetData();

        let geocode = await this.getReverseGeocode(e.latlng);

        let type = this.ITINERARY_TYPE_MAP;
        let latlng = e.latlng;
        let title = geocode && geocode.length > 0 ? geocode[0].properties.address.road : 'Default';
        let uuid = uuidv4()

        this.itinerarySteps.push({
            type: type,
            latlng: latlng,
            title: title,
            uuid: uuid,
            text: title
        });

        let marker = this.createMarker({
            type: type,
            latlng: latlng,
            title: title,
            iconSelected: true,
            uuid: uuid
        });

        this.markers.addLayer(marker);

        this.setWaypoints();
    }

    onMarkerClick(e) {
        this.resetData();

        let type = this.ITINERARY_TYPE_MARKER;
        let latlng = e.latlng;
        let title = e.target.options.title;
        let uuid = e.target.options.uuid

        this.itinerarySteps.push({
            type: type,
            latlng: latlng,
            title: title,
            uuid: uuid,
            text: title
        });

        e.target.setIcon(this.createIcon(true))

        this.setWaypoints();
    }

    onMarkerOver(e) {
        if (this.contextMenu === null) {
            this.createPopup({e: e});
        }
    }

    onMarkerOut(e) {
        this.createPopup({e: e}).close();
    }

    onMarkerRemove(evMarker, marker) {
        this.resetData()

        this.resetContextMenu();

        let count = 0;

        this.itinerarySteps.forEach((itinerary) => {
            if (itinerary.uuid === evMarker.target.options.uuid) {
                count++;
            }
        });

        this.itinerarySteps.splice(marker.index, 1);

        if (evMarker.target.options.type === this.ITINERARY_TYPE_MAP) {
            if (count === 1) this.markers.removeLayer(evMarker.target)
        } else {
            if (count === 1) evMarker.target.setIcon(this.createIcon(false));
        }

        this.setWaypoints();
    }

    async onRoutingRoutesFound(e) {
        let route = e.routes[0];
        let coordinates = route.coordinates;

        let polylines = L.polyline(coordinates, {color: 'red'}).addTo(this.map);

        this.map.setView(this.getLatLngForImage(coordinates), 11.04);

        setTimeout(async () => {
            await this.printer._print();
            polylines.removeFrom(this.map);

            this.generateRouteData(route);
        }, 250)
    }

    onContextMenuRemove() {
        this.contextMenu = null
    }

    resetUx() {
        let image = this.getTagImage();
        image.src = '';
        image.classList.add('d-none')
        image.dataset['base64'] = '';

        document.getElementById('ui_from_destination').innerHTML = '';
        document.getElementById('ui_to_destination').innerHTML = '';
        document.getElementById('ui_total_travel').innerHTML = '';
        document.getElementById('ui_table_steps').querySelector('tbody').innerHTML = '';
    }

    ui_seconds_to_hours(seconds) {
        let hours = Math.floor(seconds / 3600)
        let minutes = Math.floor((seconds % 3600) / 60)

        if (hours === 0) return `${minutes} minutes`;
        return `${hours} h ${minutes} m`;
    }

    ui_convert_to_km(value) {
        let kilometers = value / 1000;
        return `${kilometers.toFixed(2)} km`;
    }

    ui_from_destination(steps) {
        return steps[0];
    }

    ui_to_destination(steps) {
        return steps[steps.length - 1];
    }

    ui_table_row(step) {
        let [icon_text, icon_path] = this.ui_icon(step);
        let icon = icon_path && icon_text
            ? `<img style="width: 25px; height: 25px" src="${icon_path}" alt="${icon_text}">`
            : ''

        return `
            <tr>
              <td class="td-icon">${icon}</td>
              <td class="td-text">${step.text}</td>
              <td class="td-distance">${this.ui_convert_to_km(step.distance)}</td>
            </tr>
        `;
    }

    ui_icon(step) {
        
        let type = step.type;

        if (!this.AVAILABLE_ICONS.includes(type)) return [
            'Default', 
            new URL("../../../public/" + this.PUBLIC_DIRECTION_PATH + 'Default.svg', 
            import.meta.url).href
        ]

        if (type === 'EndOfRoad') {
            if (step.text.startsWith('Turn left')) type = 'Left';
            if (step.text.startsWith('Turn slight left')) type = 'SlightLeft';
            if (step.text.startsWith('Turn sharp left')) type = 'SharpLeft';
            if (step.text.startsWith('Turn right')) type = 'Right';
            if (step.text.startsWith('Turn slight right')) type = 'SlightRight';
            if (step.text.startsWith('Turn sharp right')) type = 'SharpRight';
        }

        if (type === 'Fork') {
            if (step.text.startsWith('Keep left')) type = 'Left';
            if (step.text.startsWith('Keep right')) type = 'Right';
            if (step.text.startsWith('Keep straight')) type = 'Straight';
        }

        let text = type.replace(/([A-Z])/g, ' $1').trim() + ' Image';
        let path = new URL(
            "../../../public/" + this.PUBLIC_DIRECTION_PATH + type + '.svg', 
            import.meta.url
        ).href;
    

        return [text, path]
    }

    on(action) {
        if (action === 'userAction' && typeof this.options.onUserAction === 'function') this.options.onUserAction(this);
        if (action === 'init' && typeof this.options.onInit === 'function') this.options.onInit(this);
    }

    getData() {
        return this.data;
    }

    getItinerarySteps() {
        return this.itinerarySteps;
    }

    resetData() {
        this.data = null;
        this.on('userAction');
    }

    getMarkers() {
        return this.markers;
    }

    getMarkerByLatLng(latlng) {
        let foundMarker = null;
        this.getMarkers().getLayers().forEach((marker) => {
            if (marker.getLatLng().equals(latlng)) {
                foundMarker = marker;
            }
        });
        return foundMarker;
    }

    initWithSet(data) {
        data.forEach((waypoint) => {
            let latlng = L.latLng(waypoint.latitude, waypoint.longitude);

            let foundedMarker = this.getMarkerByLatLng(latlng);

            if (foundedMarker) {
                this.itinerarySteps.push({
                    type: this.ITINERARY_TYPE_MARKER,
                    latlng: latlng,
                    title: foundedMarker.options.title,
                    uuid: foundedMarker.options.uuid,
                    text: foundedMarker.options.title
                });

                foundedMarker.setIcon(this.createIcon(true));
            } else {
                let title = waypoint.text;
                let type = this.ITINERARY_TYPE_MAP;
                let uuid = uuidv4()

                this.itinerarySteps.push({
                    type: type,
                    latlng: latlng,
                    title: title,
                    uuid: uuid,
                    text: title
                });

                let marker = this.createMarker({
                    type: type,
                    latlng: latlng,
                    title: title,
                    iconSelected: true,
                    uuid: uuid
                });

                this.markers.addLayer(marker);
            }
        })

        this.setWaypoints();
    }
}