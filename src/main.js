import './assets/main.css'

import { createApp } from 'vue'
import store from './store'

import App from './App.vue'
import router from './router'

const app = createApp(App)

import APIs from '@/services'
APIs.init(app)

/* import base components */
import Base from '@/base/main'
Base.initBaseComponents(app)


store.init(app)
app.use(router)

app.mount('#app')

window.app = app
