import { createPinia } from 'pinia'

/**
 * function to init pinia called in main.js
 * @param {*} app
 */
const init = (app) => {
    const pinia = createPinia()
    app.use(pinia)
}

// Imports here all stores !
import { useUserStore } from './modules/user'
import { useLivedata } from './modules/livedata'
import { useItineraryStore } from './modules/itinerary'

/**
 * Initializes all store modules and the endpoint of each store
 * @param {*} api
 * @returns
 */
const initStoreModels = (api, pdf) => {
    // ---- All models initialization ----
    // 'nameofmodel': [useNameOfModelStore(), api.nameofmodel or null],
    const modelsToInit = {
        user: [useUserStore(), api.auth],
        itinerary: [useItineraryStore(), pdf.itineraries],
        livedata: [useLivedata(), null],
    }
    let store = {}
    Object.keys(modelsToInit).map((model) => {
        // modelsToInit[model][0] = store
        // modelsToInit[model][1] = api
        // Setup Endpoint of each stores
        if (modelsToInit[model][1] != null) {
            modelsToInit[model][0].initStore(modelsToInit[model][1])
        }
        // Creates a new object to get only : { modelName: useModelNameStore(), ... }
        // In order to provide in App.vue
        store[model] = modelsToInit[model][0]
    })
    return store
}

/**
 * Class that provided to App.vue
 */
class Store {
    // private variable
    _store = {}

    constructor(store) {
        this.store = store
    }
    /**
     * Set the store
     */
    set store(value) {
        this._store = value
    }
    /**
     * Get a store by this name (name = id)
     * @param {*} name
     * @returns store instance
     */
    getStoreByName(name) {
        try {
            if (this._store[name] == undefined) throw new Error(`The store id "${name}" does not exist !`)
            return this._store[name]
        } catch (error) {
            console.error(error)
            return undefined
        }
    }
}

export default { init, initStoreModels, Store }
