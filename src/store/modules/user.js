import {defineStore} from 'pinia'
import {useStoreFactory} from './useStoreFactory'

const {factoryState, factoryActions, factoryGetters} = useStoreFactory()

export const useUserStore = defineStore('user', {
    state: () => ({
        ...factoryState(),
        user: null,
        isAuthenticated: false,
    }),
    actions: {
        ...factoryActions,
        /**
         * Check if user is connected (by token)
         * @returns {Boolean}
         */
        checkAuth: async function () {
            const apiToken = localStorage.getItem("apiToken");

            if (apiToken) {
                return this.endpoint
                    .verify({"token": apiToken})
                    .then((response) => {
                        console.debug('USER: ✔ auth ok', response.data)
                        this.user = response.data
                        this.isAuthenticated = true
                        return response.response
                    })
                    .catch((error) => {
                        console.debug('USER: ❌ check failed', error)
                        // commit('setIsAuthenticated', false)
                        this.isAuthenticated = false
                        return error.response
                    })
            }
        },
        /**
         * Method to connect user by the form
         * @returns {Promise<T>} or @returns {Object}
         */
        // eslint-disable-next-line no-unused-vars
        tryLogin: function (payload) {
            return this.endpoint
                .login(payload)
                .then((response) => {
                    console.debug('USER: ✔ login success', response)
                    localStorage.setItem("apiToken", response.data.token);
                    this.checkAuth()
                    return response
                }).catch((error) => {
                    console.debug('USER: ❌ check failed', error)
                    this.isAuthenticated = false
                    return error.response
                })
        },
        /**
         * Send logout to the auth api
         * @returns {Boolean} or @returns {Object}
         */
        logOut: function (payload) {
            return this.endpoint
                .logOut(payload)
                .then((response) => {
                    console.debug('USER: ✔ logout success', response)
                    this.isAuthenticated = false
                    this.user = null
                    localStorage.removeItem("apiToken");
                    location.reload()
                    return response.response
                }).catch((error) => {
                    console.debug('USER: ❌ logout failed', error)
                    this.isAuthenticated = false
                    this.user = null
                    localStorage.removeItem("apiToken");
                    location.reload()
                    return error.response
                })
        },
    },
    getters: {
        ...factoryGetters,
    }
})