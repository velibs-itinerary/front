/**
 * Function that made the inheritance / shared state, action and getters in Pinia
 * @returns store instance
 */
export function useStoreFactory() {

    const state = () => ({
        endpoint: {},
        results: [],
        resultsRaw: {},
    })

    const actions = {
        initStore(endpoint) {
            this.endpoint = endpoint
        }
    }

    const getters = {
    }

    return {
        //*================================ STATES ===============================*/
        factoryState: state,
        //*================================ ACTIONS ==============================*/
        factoryActions: actions,
        //*================================ GETTERS ==============================*/
        factorygetters: getters,
    }
}