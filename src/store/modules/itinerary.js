import { defineStore } from 'pinia'
import { useStoreFactory } from './useStoreFactory'

const { factoryState, factoryActions, factoryGetters } = useStoreFactory()

export const useItineraryStore = defineStore('itinerary', {
    state: () => ({
        ...factoryState(),
    }),
    actions: {
        ...factoryActions,
        /**
         * Get itineraries with post.... method
         */
        search: async function() {
            await this.endpoint.post({
                "token": localStorage.getItem("apiToken")
            }).then((response) => {
                console.debug('ITINERARIES: ✔ success', response)
                this.results = response.data
                // console.log("response.data.length", response.data.length);
                this.resultsRaw = response.data.length
            }).catch((error) => {
                console.debug('ITINERARIES: ❌ failed', error)
                return error.response
            })
        }
    },
    getters: {
        ...factoryGetters,
    }
})