import { defineStore } from 'pinia'

export const useLivedata = defineStore('livedata', {
    state: () => ({
        currentView: "dashboard",
        // app is ready ?
        isAppReady: false
    }),
    actions: {
        /**
         * Set the name of the current view
         * @param {*} name 
         */
        setViewName(name) {
            this.currentView = name
        },
        /**
         * Set the app ready to display
         * @param {*} name 
         */
        setAppReady(ready) {
            this.isAppReady = ready
        },
    },
    getters: {
    }
})