<p style="text-align:center!important">
<img src="https://pngimg.com/uploads/bicycle/bicycle_PNG5374.png" width="80px">
</p>
<h1 style="text-align:center!important">Velibs Itinerary - CLIENT</h1>

<div style="text-align:center!important">

[![Node](https://img.shields.io/badge/nodejs-18.13-009D28.svg?style=for-the-badge)](https://nodejs.org/en/download)
[![NPM](https://img.shields.io/badge/npm-9.2-009D28.svg?style=for-the-badge)](https://nodejs.org/en/download)
[![Vue 3](https://img.shields.io/badge/vue-3-009D28.svg?style=for-the-badge)](https://vuejs.org/)

</div>

---

<h2 id="table-des-matières">📝 Table des matières</h2>

- <a href="#table-des-matières">📝 Table des matières</a>
- <a href="#presentation-du-projet">📚 Présentation du projet</a>
- <a href="#setup-le-projet">⚙️ Setup le projet</a>
- <a href="#participants">👨‍👦‍👦 Participants</a>

<h2 id="presentation-du-projet">📚 Présentation du projet</h2>

Le projet Velibs Itinerary à été développé pour le module `API et Services web` proposé par `Livecampus`.

Dans la partie front, il était demandé de fournir les fonctionnalités suivantes :

Base: 
- Connexion, Déconnexion, Changement de mot de passe et de nom d'utilisateur
- Création, Edition, Suppresion, Téléchargement d'un itinéraire
- Affichage des stations

Extras:
- Pathfinding
- Affichage des étapes itinéraire claire
- Interface intuitive et limpid
- Pagination, Recherche, Tri 

<h2 id="setup-le-projet">⚙️ Setup le projet</h2>

Toutes les commandes réalisés ci-dessous sont issues du fichier package.json.

**Lancement du projet**

- Copier le `.env.sample` en `.env` ne modifier son contenu uniquement que si nécessaire
- Vérifier que vous utilisez une version de node/npm compatible avec celle du projet
- Installer les dépendances : `npm install`
- Lancer le projet : `npm run dev`

**Arrêt du projet**

- Couper le serveur : ⌨️ `ctrl+c`
7
<h2 id="participants">👨‍👦‍👦 Participants</h2>

**Mickaël Lebas**, **Alexis Py**, **Théo Posty**
